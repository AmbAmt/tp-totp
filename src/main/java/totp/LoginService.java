package totp;

public class LoginService {

    private final UserRepository userRepository;
    private final TotpGenerator totpGenerator;

    public LoginService(UserRepository userRepository, TotpGenerator totpGenerator){
        this.userRepository = userRepository;
        this.totpGenerator = totpGenerator;
    }

    public boolean attemptLogin(String email, String code){
       boolean test = false;
        //find user by email
        User user = userRepository.getUserByLogin(email);
        if(user != null){
           //generate dans totp generator user secret
           String generatedCode = totpGenerator.generate(user.getKey());
            //compare given code et generated code
            if(generatedCode.equals(code)){
               test = true;
           }
       }
        return test;
    }
}
