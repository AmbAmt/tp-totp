package totp;

import java.util.Random;

public class RegisterService {

    private final RandomKeyGenerator randomKeyGenerator;
    private final UserRepository userRepository;

    public RegisterService(RandomKeyGenerator randomKeyGenerator, UserRepository userRepository){
        this.randomKeyGenerator = randomKeyGenerator;
        this.userRepository = userRepository;
    }

    public String register(String email){
        String key = "compte déjà existant.";
        if(userRepository.getUserByLogin(email)==null) {
            // crée un générateur de clef aléatoire
          // RandomKeyGenerator randomKeyGenerator = new RandomKeyGenerator();
            key = randomKeyGenerator.generateKey();
            // ajoute dans le repo l'utilisateur crée
            User user = new User(email, key);
            userRepository.save(user);
        }
        return key;
    }
}
