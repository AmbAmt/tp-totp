package totp;

import java.time.Clock;
import java.time.Instant;

public class TotpGenerator {

    private Clock clock;
    public TotpGenerator(Clock clock){
        this.clock = clock;
    }

    public String generate(String key){
        String code= "";
        Long timestamp = Instant.now(clock).getEpochSecond()/30;
        code = String.valueOf((timestamp+ Integer.parseInt(key))%1000000);
        return code;
    }
}
