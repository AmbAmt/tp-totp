package totp;

import java.time.Clock;
import java.util.Locale;
import java.util.Random;
import java.util.Scanner;

public class main {
    /*
    to do :
     */
    private static Scanner sc = new Scanner(System.in);
    private static final UserRepository userRepository = new UserRepository();
    private static final RandomKeyGenerator randomKeyGenerator = new RandomKeyGenerator(new Random());
    private static final RegisterService registerService = new RegisterService(randomKeyGenerator, userRepository);
    private static final TotpGenerator totpGenerator = new TotpGenerator( Clock.systemDefaultZone());
    private static final LoginService loginService = new LoginService(userRepository, totpGenerator);

    public static void main(String[] args) {
        String test = "";
        do {
            System.out.println("Que souhaitez vous faire ? REGISTER / GENERATE / LOGIN / STOP ");
            test = sc.nextLine().toUpperCase();
            switch (test) {
                case "REGISTER" -> register();
                case "GENERATE" -> generate();
                case "LOGIN" -> login();
                default -> test = "STOP";
            }
        } while (!test.equals("STOP"));
        System.out.println("Au revoir !");
    }

    public static void register() {
        //saisie de l'email par l'utilisateur
        System.out.println("Bonjour,quel est votre email ?");
        //   Long random = new Random().nextLong();
        //enregistre l'adresse mail de l'utilisateur
        String key = registerService.register(sc.nextLine());
        System.out.println("Votre clef est : " + key);

        System.out.println(userRepository.getUsers());
    }

    public static void generate() {
        System.out.println("Quel est votre clef ?");
        System.out.println("Votre code est : " + totpGenerator.generate(sc.nextLine()));
    }

    public static void login() {
        System.out.println("Quel est votre login ?");
        String login = sc.nextLine();
        System.out.println("Quel est votre code ?");
        if(loginService.attemptLogin(login, sc.nextLine())){
            System.out.println("Vous êtes authentifier.");
        }else {
            System.out.println("Un erreur s'est produite.");
        }
    }
}
