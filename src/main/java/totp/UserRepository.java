package totp;

import java.util.HashMap;

public class UserRepository {
    private static HashMap<String, User> users = new HashMap<>();

    public void save(User user) {
        //on stock en clef le login et en valeur l'objet user
        this.users.put(user.getLogin(), user);
    }

    public static HashMap getUsers() {
        return users;
    }

    //trouver un utilisateur par le biais de son email
    public User getUserByLogin(String login) {
        User user = users.get(login);
        return user;
    }
}
