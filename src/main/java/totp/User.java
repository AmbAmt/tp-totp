package totp;

import java.util.HashMap;
import java.util.Objects;

public class User {
   String login;
   String key;

    public User(String login, String key){
        this.login = login;
        this.key = key;
    }

    public String getLogin() {
        return login;
    }

    public String getKey() {
        return key;
    }

  /*  public boolean equals(Object o){
        if(!(o instanceof User other)){
            return false;
        }else{
            return other.getLogin().equals(this.login) && other.getKey().equals(this.key);
        }
    }*/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(login, user.login) && Objects.equals(key, user.key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(login, key);
    }

    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                ", key='" + key + '\'' +
                '}';
    }
}
