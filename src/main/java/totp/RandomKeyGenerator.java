package totp;

import java.util.Random;

public class RandomKeyGenerator {

    private Random random;

    public RandomKeyGenerator(Random random){
        this.random = random;
    }

    public String generateKey(){
        //genère une chaine aléatoire
        int i = 0;
        String key = "";
        while (i  < 10){
            key += random.nextInt(0,9);
            i++;
        }
        return key;
    }
}
