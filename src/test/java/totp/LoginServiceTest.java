package totp;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;

public class LoginServiceTest {
    @Test
    void attemptLoginTest(){
        UserRepository userRepositoryMock = Mockito.mock(UserRepository.class);
        TotpGenerator totpGenerator = Mockito.mock(TotpGenerator.class);
        LoginService loginService = new LoginService(userRepositoryMock,totpGenerator);
        Mockito.when(userRepositoryMock.getUserByLogin("amat.ambre@gmail.com")).thenReturn(new User("amat.ambre@gmail.com", "1234567890"));
        Mockito.when(totpGenerator.generate("1234567890")).thenReturn("123456");
        assertTrue(loginService.attemptLogin("amat.ambre@gmail.com", "123456"));
        assertFalse(loginService.attemptLogin("amt.ambre@gmail.com", "123456"));
        assertFalse(loginService.attemptLogin("amat.ambre@gmail.com", "12356"));
    }

}
