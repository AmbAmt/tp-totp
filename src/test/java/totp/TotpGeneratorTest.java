package totp;

import org.junit.jupiter.api.Test;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneOffset;

import static org.junit.jupiter.api.Assertions.*;

public class TotpGeneratorTest {
    @Test
    void generateTest() {
        TotpGenerator totpGenerator = new TotpGenerator(Clock.fixed(Instant.EPOCH, ZoneOffset.UTC));
        assertEquals("567890", totpGenerator.generate("1234567890"));
    }
}
