package totp;

import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

public class RandomKeyGeneratorTest {
    RandomKeyGenerator randomKeyGenerator = new RandomKeyGenerator(new FakeRandom());

    @Test
    void generateKeyTest() {
        String test = randomKeyGenerator.generateKey();
        assertEquals(10, test.length());
        assertTrue(test.matches("[0-9]{10}"));
        assertEquals("1111111111", test);
    }
}

class FakeRandom extends Random {
    @Override
   public int nextInt(int origin, int bound) {
        return 1;
    }

}
