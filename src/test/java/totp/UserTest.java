package totp;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
public class UserTest {

    @Test
    void getLoginTest(){
        User user = new User("Login", "Key");
        assertEquals("Login", user.getLogin());
    }

    @Test
    void getKeyTest(){
        User user = new User("Login", "Key");
        assertEquals("Key", user.getKey());
    }
}
