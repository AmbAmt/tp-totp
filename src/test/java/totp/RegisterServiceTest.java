package totp;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

public class RegisterServiceTest {

    @Test
    void nominalCase() {
        // GIVEN
        RandomKeyGenerator randomKeyGeneratorMock = Mockito.mock(RandomKeyGenerator.class);
        UserRepository userRepositoryMock = Mockito.mock(UserRepository.class);

        Mockito.when(randomKeyGeneratorMock.generateKey()).thenReturn("12345678");

        RegisterService registerService = new RegisterService(randomKeyGeneratorMock, userRepositoryMock);

        // WHEN
        final String secretKey = registerService.register("ba@zenika.com");

        // THEN
        assertEquals("12345678", secretKey);
     //   Mockito.verify(userRepositoryMock).save(Mockito.eq(new User("ba@zenika.com", "12345678")));
    }


   /* @Test
    void registerTest() {
        //vérifie qu'on a bien le même type
        assertEquals("String".getClass(), registerService.register("test").getClass());
    }
    //vérifier l'enregistrement de deux personnes

    @Test
    void registerTwoUserTest() {
        registerService.register("1");
        registerService.register("2");
        assertEquals(3, UserRepository.getUsers().size());
    }

    //essayer de s'enregister si il y a dejà qu'elqu'un avec cet email

    @Test
    void registerExistingTest() {
        assertEquals("compte déjà existant.", registerService.register("1"));
    }*/
}
