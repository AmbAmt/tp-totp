package totp;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class UserRepositoryTest {
    //vérifier la non régression

    //test d'un utilisateur
    @Test
    void saveOneUser() {
        User user1 = new User("Login1", "Key1");
        UserRepository userRepository = new UserRepository();

        userRepository.save(user1);

        assertEquals("{Login1=User{login='Login1', key='Key1'}}", userRepository.getUsers().toString());
    }

    //test de deux utilisateurs
    @Test
    void saveTwoUsers() {
        User user1 = new User("Login1", "Key1");
        User user2 = new User("Login2", "Key2");
        UserRepository userRepository = new UserRepository();

        userRepository.save(user1);
        userRepository.save(user2);

        assertEquals("{Login1=User{login='Login1', key='Key1'}, Login2=User{login='Login2', key='Key2'}}", userRepository.getUsers().toString());
    }

    @Test
    void getUserByLoginTest() {
        User user1 = new User("Login1", "Key1");
        User user2 = new User("Login2", "Key2");
        UserRepository userRepository = new UserRepository();

        userRepository.save(user1);
        userRepository.save(user2);

        assertEquals("Login1", userRepository.getUserByLogin("Login1").getLogin());
        assertEquals("Key1", userRepository.getUserByLogin("Login1").getKey());
        assertEquals("Key2", userRepository.getUserByLogin("Login2").getKey());
        assertEquals(null, userRepository.getUserByLogin("Login5"));

    }
}
